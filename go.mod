module gitlab.com/arispriyatna95/buayagomod

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/stretchr/testify v1.7.0
	github.com/subosito/gotenv v1.2.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
