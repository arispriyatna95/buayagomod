package log

import (
	"testing"
)


func TestConfigLog(t *testing.T) {
	infoLog := "info"
	warningLog := "warning"
	errorLog := "error"

	InfoLogger.Printf("Test Info Log %s", infoLog)
	WarningLogger.Printf("Test Warning Log %s", warningLog)
	ErrorLogger.Printf("Test Error Log %s", errorLog)
}
