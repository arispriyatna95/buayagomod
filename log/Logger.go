package log

import (
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strconv"

	"github.com/subosito/gotenv"
	"gopkg.in/natefinch/lumberjack.v2"
)

var (
	WarningLogger *log.Logger
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
)

func init() {

	dirname, errDir := os.Getwd()
	if errDir != nil {
		panic(errDir)
	}

	if runtime.GOOS == "windows" {
		dir, errJoin := os.Open(filepath.Join(dirname, "..\\"))
		if errJoin != nil {
			panic(errJoin)
		}

		err := gotenv.Load(dir.Name() + "\\.env.example")

		if err != nil {
			panic(err)
		}
	} else {
		dir, errJoin := os.Open(path.Join(dirname, "../"))
		if errJoin != nil {
			panic(errJoin)
		}

		err := gotenv.Load(dir.Name() + ".env.example")

		if err != nil {
			panic(err)
		}
	}

	logFile := os.Getenv("LOG_FILE_LOCATION")
	logSize := os.Getenv("LOG_FILE_MAX_SIZE")
	logMaxBackup := os.Getenv("LOG_FILE_MAX_BACKUP")

	logSizeConvert, err := strconv.Atoi(logSize)
	if err != nil {
		panic(err.Error())
	}

	logBackupConvert, errBackupConvert := strconv.Atoi(logMaxBackup)
	if errBackupConvert != nil {
		panic(errBackupConvert.Error())
	}

	ConfigLog(logFile, logSizeConvert, logBackupConvert)
}

func ConfigLog(logFile string, logSize, logMaxBackup int) {
	if logFile != "" {
		userFile := &lumberjack.Logger{
			Filename:   logFile,
			MaxSize:    logSize,
			MaxBackups: logMaxBackup,
		}
		InfoLogger = log.New(userFile, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
		WarningLogger = log.New(userFile, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
		ErrorLogger = log.New(userFile, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	}
}
