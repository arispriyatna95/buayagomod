package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

var route = Request{}

var boolSetup bool = false

type Router struct {
}

func (r Router) Add(request []Add) error{

	if !boolSetup {

		return fmt.Errorf("Please Setup Base Router First ")
	}

	for i := 0; i < len(request); i++ {
		route.AddEndpoint = append(route.AddEndpoint, request[i])
	}

	return nil


}

func (r Router) Start() error {

	if !boolSetup {

		return fmt.Errorf("Please Setup Base Router First ")
	}

	router := gin.Default()

	gin.SetMode(route.ReleaseMode)

	if route.UseJwt {
		router.GET("/generate", authJwt)
	}

	if len(route.AddEndpoint) > 0 {

		for i := 0; i < len(route.AddEndpoint); i++ {

			if route.AddEndpoint[i].Method == "POST" {

				if !route.UseJwt {
					router.POST(route.AddEndpoint[i].Path, route.AddEndpoint[i].Handler)

				} else {
					router.POST(route.AddEndpoint[i].Path, AuthMiddleware(), route.AddEndpoint[i].Handler)
				}

			} else if route.AddEndpoint[i].Method == "GET" {

				if !route.UseJwt {
					router.GET(route.AddEndpoint[i].Path, route.AddEndpoint[i].Handler)
				} else {
					router.GET(route.AddEndpoint[i].Path, AuthMiddleware(), route.AddEndpoint[i].Handler)
				}

			} else if route.AddEndpoint[i].Method == "PUT" {

				if !route.UseJwt {
					router.PUT(route.AddEndpoint[i].Path, route.AddEndpoint[i].Handler)
				} else {
					router.PUT(route.AddEndpoint[i].Path, AuthMiddleware(), route.AddEndpoint[i].Handler)
				}

			} else if route.AddEndpoint[i].Method == "DELETE" {

				if !route.UseJwt {
					router.DELETE(route.AddEndpoint[i].Path, route.AddEndpoint[i].Handler)
				} else {
					router.DELETE(route.AddEndpoint[i].Path, AuthMiddleware(), route.AddEndpoint[i].Handler)
				}
			}
		}
	}else {
		if !route.UseJwt {
			router.GET("/api/v1/get", get)
		} else {
			router.GET("/api/v1/get", AuthMiddleware(), get)
		}
	}

	router.Run(route.Port)

	return nil

}

func (r Router) Setup(detail Request) error {
	boolSetup = true

	err :=  detail.ValidateSetupRouter()

	if err != nil {
		return err
	}

	route.Port = detail.Port
	route.ReleaseMode = detail.ReleaseMode
	route.UseJwt = detail.UseJwt

	return nil
}

func get(c *gin.Context)  {

	c.JSON(200, "SUCCESS")
}

func authJwt(c *gin.Context)  {

	var middlewareResponse = MiddlewareResponse{Key: c.PostForm("key"), Value: c.PostForm("value")}

	token, err := GenerateToken(&middlewareResponse)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"messages": "error generate token"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": token})

}

func CreateRouter() InterfaceRouter  {
	return &Router{}
}
