package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation"
)

type Request struct {
	ReleaseMode 	string
	Port 			string
	UseJwt			bool
	AddEndpoint 	[]Add


}

type Add struct {
	Method	 string
	Path string
	Handler func(c *gin.Context)
}

type MiddlewareResponse struct {
	Key 	string
	Value 	string
}

func (u *Request) ValidateSetupRouter() error {
	if err := validation.Validate(u.Port, validation.Required); err != nil {
		return fmt.Errorf("Port cannot be blank ")
	}

	if err := validation.Validate(u.ReleaseMode, validation.Required); err != nil {
		return fmt.Errorf("Release Mode Cannot be blank ")
	}

	return nil
}
