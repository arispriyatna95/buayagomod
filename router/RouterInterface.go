package router

type InterfaceRouter interface {

	Setup(detail Request) error
	Start() error
	Add(request []Add) error

}
