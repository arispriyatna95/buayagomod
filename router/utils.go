package router

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/subosito/gotenv"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

func init()  {
	dirname, errDir := os.Getwd()
	if errDir != nil {
		panic(errDir)
	}

	if runtime.GOOS == "windows" {
		dir, errJoin := os.Open(filepath.Join(dirname, "..\\"))
		if errJoin != nil {
			panic(errJoin)
		}

		err := gotenv.Load(dir.Name()+"\\.env.example")

		if err != nil {
			panic(err)
		}
	}else {
		dir, errJoin := os.Open(path.Join(dirname, "../"))
		if errJoin != nil {
			panic(errJoin)
		}

		err := gotenv.Load(dir.Name()+".env.example")

		if err != nil {
			panic(err)
		}
	}
}

func GenerateToken(response *MiddlewareResponse) (string, error) {

	secret := os.Getenv("SECRET")

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"key":  response.Key,
		"value": response.Value,
		"exp": time.Now().Add(time.Minute * 3).Unix(), //Token expires after 15 minute
	})

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Fatalln(err)
	}

	return tokenString, nil

}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := TokenValid(c.Request)
		bearerToken := c.Request.Header.Get("Authorization")
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"status": http.StatusUnauthorized,
				"error":  "Token Unauthorized",
				"description": err.Error(),
				"token": strings.Split(bearerToken, " ")[1],
			})
			c.Abort()
			return
		}

		c.Next()
	}
}

func Pretty(data interface{}) {
	b, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Println(string(b))
}

func TokenValid(r *http.Request) error {
	secret := os.Getenv("SECRET")
	tokenString := ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v ", token.Header["alg"])
		}
		return []byte(secret), nil
	})
	if err != nil {
		return err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		Pretty(claims)
	}
	return nil
}

func ExtractToken(r *http.Request) string {
	keys := r.URL.Query()
	token := keys.Get("token")
	if token != "" {
		return token
	}
	bearerToken := r.Header.Get("Authorization")
	if len(strings.Split(bearerToken, " ")) == 2 {
		return strings.Split(bearerToken, " ")[1]
	}
	return ""
}

func ExtractTokenData(r *http.Request) (*MiddlewareResponse, error) {
	secret := os.Getenv("SECRET")
	tokenString := ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v ", token.Header["alg"])
		}
		return []byte(secret), nil
	})
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	user := new(MiddlewareResponse)
	if !ok || !token.Valid {
		return nil, fmt.Errorf("You Are Not Authorized ")
	}

	b, err := json.MarshalIndent(claims, "", " ")
	if err != nil {
		log.Println(err)
		return nil, err
	}

	err = json.Unmarshal([]byte(b), &user)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return user, nil
}
