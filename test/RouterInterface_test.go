package test

import (
	"testing"

	"gitlab.com/arispriyatna95/buayagomod/router"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
)

func getHandler(c *gin.Context) {

	c.JSON(200, "SUCCESS GET HANDLER")
}

func TestRouterSetupTrue(t *testing.T) {
	err := router.CreateRouter().Setup(router.Request{Port: ":9096", ReleaseMode: "debug", UseJwt: true})

	require.Nil(t, err)

	var messages = []router.Add{
		{
			Method:  "GET",
			Path:    "/api/v1/get",
			Handler: getHandler,
		},
		{
			Method:  "POST",
			Path:    "/api/v1/post",
			Handler: getHandler,
		},
		{
			Method:  "PUT",
			Path:    "/api/v1/put",
			Handler: getHandler,
		},
		{
			Method:  "DELETE",
			Path:    "/api/v1/delete",
			Handler: getHandler,
		},
	}

	errAdd := router.CreateRouter().Add(messages)

	require.Nil(t, errAdd)

	errStart := router.CreateRouter().Start()

	require.Nil(t, errStart)
}

func TestRouterSetupWrong(t *testing.T) {

	var messages = []router.Add{
		{
			Method:  "GET",
			Path:    "/api/v1/get",
			Handler: getHandler,
		},
		{
			Method:  "POST",
			Path:    "/api/v1/post",
			Handler: getHandler,
		},
		{
			Method:  "PUT",
			Path:    "/api/v1/put",
			Handler: getHandler,
		},
		{
			Method:  "DELETE",
			Path:    "/api/v1/delete",
			Handler: getHandler,
		},
	}

	errAdd := router.CreateRouter().Add(messages)

	require.Nil(t, errAdd)

	err := router.CreateRouter().Setup(router.Request{Port: ":9096", ReleaseMode: "debug", UseJwt: true})

	require.Nil(t, err)

	errStart := router.CreateRouter().Start()

	require.Nil(t, errStart)

}

func TestRouterWithPortBlank(t *testing.T) {
	err := router.CreateRouter().Setup(router.Request{Port: "", ReleaseMode: "debug", UseJwt: true})

	require.Nil(t, err)

	var messages = []router.Add{
		{
			Method:  "GET",
			Path:    "/api/v1/get",
			Handler: getHandler,
		},
		{
			Method:  "POST",
			Path:    "/api/v1/post",
			Handler: getHandler,
		},
		{
			Method:  "PUT",
			Path:    "/api/v1/put",
			Handler: getHandler,
		},
		{
			Method:  "DELETE",
			Path:    "/api/v1/delete",
			Handler: getHandler,
		},
	}

	errAdd := router.CreateRouter().Add(messages)

	require.Nil(t, errAdd)

	errStart := router.CreateRouter().Start()

	require.Nil(t, errStart)

}

func TestRouterWithReleaseModeBlank(t *testing.T) {
	err := router.CreateRouter().Setup(router.Request{Port: ":9096", ReleaseMode: "", UseJwt: true})

	require.Nil(t, err)

	var messages = []router.Add{
		{
			Method:  "GET",
			Path:    "/api/v1/get",
			Handler: getHandler,
		},
		{
			Method:  "POST",
			Path:    "/api/v1/post",
			Handler: getHandler,
		},
		{
			Method:  "PUT",
			Path:    "/api/v1/put",
			Handler: getHandler,
		},
		{
			Method:  "DELETE",
			Path:    "/api/v1/delete",
			Handler: getHandler,
		},
	}

	errAdd := router.CreateRouter().Add(messages)

	require.Nil(t, errAdd)

	errStart := router.CreateRouter().Start()

	require.Nil(t, errStart)

}

func TestRouterWithJwt(t *testing.T) {
	err := router.CreateRouter().Setup(router.Request{Port: ":9096", ReleaseMode: "debug", UseJwt: true})

	require.Nil(t, err)

	var messages = []router.Add{
		{
			Method:  "GET",
			Path:    "/api/v1/get",
			Handler: getHandler,
		},
		{
			Method:  "POST",
			Path:    "/api/v1/post",
			Handler: getHandler,
		},
		{
			Method:  "PUT",
			Path:    "/api/v1/put",
			Handler: getHandler,
		},
		{
			Method:  "DELETE",
			Path:    "/api/v1/delete",
			Handler: getHandler,
		},
	}

	errAdd := router.CreateRouter().Add(messages)

	require.Nil(t, errAdd)

	errStart := router.CreateRouter().Start()

	require.Nil(t, errStart)

}

func TestRouterWithoutJwt(t *testing.T) {
	err := router.CreateRouter().Setup(router.Request{Port: ":9096", ReleaseMode: "debug", UseJwt: false})

	require.Nil(t, err)

	var messages = []router.Add{
		{
			Method:  "GET",
			Path:    "/api/v1/get",
			Handler: getHandler,
		},
		{
			Method:  "POST",
			Path:    "/api/v1/post",
			Handler: getHandler,
		},
		{
			Method:  "PUT",
			Path:    "/api/v1/put",
			Handler: getHandler,
		},
		{
			Method:  "DELETE",
			Path:    "/api/v1/delete",
			Handler: getHandler,
		},
	}

	errAdd := router.CreateRouter().Add(messages)

	require.Nil(t, errAdd)

	errStart := router.CreateRouter().Start()

	require.Nil(t, errStart)

}

func TestRouterDefaultEndpointWithJwt(t *testing.T) {
	err := router.CreateRouter().Setup(router.Request{Port: ":9096", ReleaseMode: "debug", UseJwt: true})

	require.Nil(t, err)

	errStart := router.CreateRouter().Start()

	require.Nil(t, errStart)

}

func TestRouterDefaultEndpointWithoutJwt(t *testing.T) {
	err := router.CreateRouter().Setup(router.Request{Port: ":9096", ReleaseMode: "debug", UseJwt: false})

	require.Nil(t, err)

	errStart := router.CreateRouter().Start()

	require.Nil(t, errStart)

}
