package test

import (
	"testing"

	"gitlab.com/arispriyatna95/buayagomod/log"
)

func TestConfigLog(t *testing.T) {
	infoLog := "info"
	warningLog := "warning"
	errorLog := "error"

	log.InfoLogger.Printf("Test Info Log %s", infoLog)
	log.WarningLogger.Printf("Test Warning Log %s", warningLog)
	log.ErrorLogger.Printf("Test Error Log %s", errorLog)
}
